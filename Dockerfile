FROM openjdk:8u332-jre-slim-buster

COPY target/*.jar app.jar

ENTRYPOINT ["java","-jar","/app.jar"]
